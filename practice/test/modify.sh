#!/bin/bash
FLOOR=1;
CEILING=11;
RANGE=$(($CEILING-$FLOOR+1))
files=$(find . -type f -name 'new_txt*.txt')

for f in ${files}
do
	let "RESULT %= $RANGE"
	RESULT=$(($RESULT+$FLOOR))	
	echo $RESULT
	touch -t $(date -v -1y -r $(stat -f %m  "${f}") +%Y%m%d%H%M.%S) "${f}"
	touch -t $(date -v -${RESULT}m -r $(stat -f %m  "${f}") +%Y%m%d%H%M.%S) "${f}"
#	touch -m -a -t 201812180130.09 files
done
